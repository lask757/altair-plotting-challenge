# Submission to DesertPy's plotting library challenge
#### [Meetup Link](https://www.meetup.com/PyData-Phoenix/events/267759899/), [Github repo](https://github.com/pydata-phoenix/battle-of-the-plotting-libraries)

### 1. [Time Series](https://altair-plotting.s3.us-east-2.amazonaws.com/plots/01_time_series_plot.html)
#### [Script](https://gitlab.com/lask757/altair-plotting-challenge/blob/master/src/01_time_series_plot.py)

### 2. [Categorical Bar Chart](https://altair-plotting.s3.us-east-2.amazonaws.com/plots/02_plot_categorical.html)
#### [Script](https://gitlab.com/lask757/altair-plotting-challenge/blob/master/src/02_plot_categorical.py)

### 3. [Categorical Bar Chart](https://altair-plotting.s3.us-east-2.amazonaws.com/plots/03_accident_frequency.html)
#### [Script](https://gitlab.com/lask757/altair-plotting-challenge/blob/master/src/03_accident_frequency.py)

### 4. [Geographical Plot](https://altair-plotting.s3.us-east-2.amazonaws.com/plots/04_geographical_plot.html)
#### [Script](https://gitlab.com/lask757/altair-plotting-challenge/blob/master/src/04_geographical_plot.py)

### 5. [Multivariate Plot](https://altair-plotting.s3.us-east-2.amazonaws.com/plots/05_multivariate_plot.html)
#### [Script](https://gitlab.com/lask757/altair-plotting-challenge/blob/master/src/05_multiviate_plot.py)

### 6. [Muliple Distrobutions](https://altair-plotting.s3.us-east-2.amazonaws.com/plots/06_multiple_distrobution.html)
#### [Script](https://gitlab.com/lask757/altair-plotting-challenge/blob/master/src/06_multiple_distrobution_plot.py)

### 7. [Show PNG images](https://altair-plotting.s3.us-east-2.amazonaws.com/plots/07_plot_image.html)
#### [Script](https://gitlab.com/lask757/altair-plotting-challenge/blob/master/src/07_plot_image.py)

### 8. [Rigde Plot](https://altair-plotting.s3.us-east-2.amazonaws.com/plots/08_ridge_polt.html)
#### [Script](https://gitlab.com/lask757/altair-plotting-challenge/blob/master/src/08_ridge_plot.py)
