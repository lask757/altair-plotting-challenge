from pathlib import Path

import altair as alt
import pandas as pd

DATA_DIR = Path.cwd().parent.joinpath('data')

aapl = pd.read_csv(DATA_DIR.joinpath('AAPL.csv'))
goog = pd.read_csv(DATA_DIR.joinpath('GOOG.csv'))
ibm = pd.read_csv(DATA_DIR.joinpath('IBM.csv'))
msft = pd.read_csv(DATA_DIR.joinpath('MSFT.csv'))

aapl['Name'] = 'Apple'
goog['Name'] = 'Google'
ibm['Name'] = 'IBM'
msft['Name'] = 'Microsoft'

data = pd.concat([aapl, goog, ibm, msft])

selection = alt.selection_multi(fields=['Name'], bind='legend')

chart = (alt.Chart(data, title='Historical Tech Stock Prices').mark_line()
         .encode(alt.X('Date:T', axis=alt.Axis(labelAngle=-45, format='%x')),
                 alt.Y('Adj Close:Q', title='Adjusted Close'),
                 alt.Tooltip(['Name', 'Open', 'High', 'Low',
                              'Close', 'Volume', 'Adj Close']),
                 alt.Color('Name:N', scale=alt.Scale()),
                 opacity=alt.condition(selection, alt.value(1),
                                       alt.value(.2)))
         .add_selection(selection)
         .properties(width=1_000, height=750))

chart.save(str(DATA_DIR.parent.joinpath('plots').joinpath(
    '01_time_series_plot.html')))
