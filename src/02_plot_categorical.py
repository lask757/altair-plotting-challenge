from pathlib import Path

import altair as alt
import pandas as pd

DATA_DIR = Path.cwd().parent.joinpath('data')
CRIMES = ('ARSON', 'MOTOR VEHICLE THEFT', 'DRUG OFFENSE')
INT_TO_DAY = {0: 'Monday', 1: 'Tuesday', 2: 'Wednesday', 3: 'Thursday',
              4: 'Friday', 5: 'Saturday', 6: 'Sunday'}

dtypes = {'INC NUMBER': object, 'UCR CRIME CATEGORY': object,
          '100 BLOCK ADDRESS': object, 'ZIP': float, 'PREMISE TYPE': object}

phx_crimes = pd.read_csv(DATA_DIR.joinpath(
    'crime-data_crime-data_crimestat.csv'),
    dtype=dtypes,
    parse_dates=['OCCURRED ON', 'OCCURRED TO'])

phx_crimes.columns = \
    [col.lower().replace(' ', '_') for col in phx_crimes.columns]

crimes_df = (phx_crimes[phx_crimes.ucr_crime_category.isin(CRIMES)]
             .reset_index(drop=True)
             .copy())

crimes_df['DOW'] = crimes_df.occurred_on.dt.weekday
crimes_df['HOUR'] = crimes_df.occurred_on.dt.hour

arson = (crimes_df[crimes_df.ucr_crime_category == CRIMES[0]]
         .groupby(['DOW', 'HOUR'])
         .size())

gta = (crimes_df[crimes_df.ucr_crime_category == CRIMES[1]]
       .groupby(['DOW', 'HOUR'])
       .size())

drug = (crimes_df[crimes_df.ucr_crime_category == CRIMES[2]]
        .groupby(['DOW', 'HOUR'])
        .size())

combined = pd.concat([arson, gta, drug], axis=1, keys=CRIMES)
df = combined.stack().reset_index()
df = df.rename(columns={'level_2': 'Offense', 0: 'Count'})
df['DOW'] = df.DOW.map(INT_TO_DAY)
df['Offense'] = df.Offense.apply(lambda x: x.title())

selection = alt.selection_multi(fields=['Offense'], bind='legend')

crime = (alt.Chart(df, title='Phoenix Crime')
         .mark_bar().encode(
    alt.X('Offense:N', title=None),
    alt.Y('Count:Q', title='Count of Crimes'),
    alt.Column('DOW:O', title='Day of the Week', sort=['Monday', 'Tuesday',
                                                       'Wednesday', 'Thursday',
                                                       'Friday', 'Saturday',
                                                       'Sunday']),
    alt.Color('Offense:N', scale=alt.Scale()),
    opacity=(alt.condition(selection,
                           alt.value(1), alt.value(.01))))
         .add_selection(selection)
         .properties(height=500))

out = DATA_DIR.parent.joinpath('plots').joinpath('02_plot_categorical.html')
crime.save(str(out))
