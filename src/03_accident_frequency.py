from pathlib import Path

import altair as alt
import pandas as pd

DATA_DIR = Path.cwd().parent.joinpath('data')
INT_TO_DAY = {0: 'Monday', 1: 'Tuesday', 2: 'Wednesday', 3: 'Thursday',
              4: 'Friday', 5: 'Saturday', 6: 'Sunday'}

mesa = pd.read_csv(DATA_DIR.joinpath('mesa.csv'), index_col=0)
mesa.columns = [col.title().replace(' ', '_') for col in mesa.columns]

accidents = (mesa[mesa.Event_Type_Description.str.contains('ACCIDENT')]
             .reset_index()
             .copy())

accidents['Creation_Datetime'] = pd.to_datetime(accidents.Creation_Datetime)
accidents['Day_Of_Week'] = accidents.Creation_Datetime.dt.weekday

grouped = (accidents.groupby(['Event_Type_Description', 'Day_Of_Week'])
           .size()
           .reset_index(name='Count'))
grouped['Day_Of_Week'] = grouped.Day_Of_Week.map(INT_TO_DAY)

selection = alt.selection_multi(fields=['Event_Type_Description'],
                                bind='legend')

chart = (alt.Chart(grouped, title='Mesa Accidents by Day of the Week')
         .mark_bar()
         .encode(alt.X('Day_Of_Week:O', title='Day of Week',
                       sort=['Monday', 'Tuesday', 'Wednesday', 'Thursday',
                             'Friday', 'Saturday', 'Sunday']),
                 alt.Y('Count:Q', title='Count'),
                 alt.Tooltip(['Count:Q']),
                 alt.Color('Event_Type_Description:N',
                           title='Event Type Description'),
                 opacity=alt.condition(selection,
                                       alt.value(1), alt.value(.35)))
         .add_selection(selection)
         .properties(height=800, width=1_000)
         .configure_axis(labelFontSize=14, titleFontSize=20)
         .configure_title(fontSize=24))

chart.save(str(DATA_DIR.parent.joinpath('plots').joinpath(
    '03_accident_frequency.html')))
