import json
from pathlib import Path

import altair as alt
import geopandas as gpd
import pandas as pd

PROJECT_DIR = Path().cwd().parent
UNEMPLOYMENT_COLS = ('id', 'state_num', 'county_num', 'state_county', 'year',
                     'population', 'employed', 'unemployed', 'unemployed_perc')

unemployment = \
    pd.read_csv(PROJECT_DIR.joinpath('data').joinpath('unemployment09.csv'),
                header=None, names=UNEMPLOYMENT_COLS)

az_unemployment = \
    unemployment[unemployment['state_num'] == 4].copy().reset_index(drop=True)


shape = gpd.read_file(
    str(PROJECT_DIR.joinpath('data').joinpath('cb_2018_us_county_5m.shp')))

az_shape = shape[shape['STATEFP'] == '04'].copy()
assert az_shape.shape[0] == az_unemployment.shape[0]

az_shape['COUNTYFP'] = az_shape['COUNTYFP'].astype(int)

az = az_shape.merge(az_unemployment, left_on='COUNTYFP', right_on='county_num',
                    how='inner')

to_drop = ['STATEFP', 'COUNTYFP', 'AFFGEOID', 'GEOID', 'LSAD', 'state_county',
           'LSAD', 'ALAND', 'year', 'AWATER', 'state_num', 'id', 'county_num',
           'COUNTYNS']

az = az.drop(to_drop, axis=1)

choro_json = json.loads(az.to_json())
choro_data = alt.Data(values=choro_json['features'])

base = alt.Chart(choro_data, title='2009 Arizona Unemployment').mark_geoshape(
    stroke='black',
    strokeWidth=1
).encode().properties(height=1_000, width=1_000)

choro = alt.Chart(choro_data).mark_geoshape(
    stroke='black'
).encode(
    alt.Color('properties.unemployed_perc:Q', title='Unemployment %'),
    tooltip=[alt.Tooltip('properties.NAME:N', title='County'),
             alt.Tooltip('properties.population:N', title='Workforce size'),
             alt.Tooltip('properties.employed:N', title='Employed'),
             alt.Tooltip('properties.unemployed:N', title='Unemployed'),
             alt.Tooltip('properties.unemployed_perc:Q', title='Unemployed %')]
)

chart = base + choro
chart.properties(height=1_000, width=1_000)
chart.save(
    str(PROJECT_DIR.joinpath('plots').joinpath('04_geographical_plot.html')))
