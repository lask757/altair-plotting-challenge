from pathlib import Path

import altair as alt
import pandas as pd

PROJECT_DIR = Path.cwd().parent

iris = pd.read_csv(PROJECT_DIR.joinpath('data').joinpath('iris.csv'))

iris['species'] = iris.species.apply(lambda x: x.split('-')[-1])

cols = [*iris.columns[:-1]]

brush = alt.selection(type='interval', resolve='global')

chart = alt.Chart(iris).mark_circle().encode(
    alt.X(alt.repeat('column'), type='quantitative'),
    alt.Y(alt.repeat('row'), type='quantitative'),
    alt.Color('species:N', title='Species'),
    opacity=alt.condition(brush, alt.value(1.), alt.value(.10))
).properties(
    width=200,
    height=200,
).repeat(
    row=cols,
    column=cols[::-1]
).add_selection(brush)

chart.save(
    str(PROJECT_DIR.joinpath('plots').joinpath('05_multivariate_plot.html')))
