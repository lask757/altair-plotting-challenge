from pathlib import Path

import altair as alt
import numpy as np
import pandas as pd

PROJECT_DIR = Path.cwd().parent

temps = (pd.read_csv(
    PROJECT_DIR.joinpath('data').joinpath('phoenix_maximum_daily_temps.csv'),
    index_col='Year', na_values='M'))

temps = temps.astype(np.float32)
cols_with_na = [col for col in temps if temps[col].isna().sum()]

for col in cols_with_na:
    temps[col] = temps[col].fillna(temps[col].median())

months = [*temps.columns]

box = alt.Chart(temps).transform_fold(
    months, as_=['Month', 'values']
).mark_boxplot().encode(
    alt.X('Month:O', sort=months),
    alt.Y('values:Q', scale=alt.Scale(zero=False), title='Temperature'),
).properties(
    height=450,
    width=600,
)

kde = alt.Chart(temps).transform_fold(
    months, as_=['Month', 'values']
).transform_density(
    'values',
    as_=['values', 'density'],
    groupby=['Month'],
    counts=False,
    steps=1000
).mark_area().encode(
    alt.X('values:Q', title='Temperature'),
    alt.Y('density:Q', scale=alt.Scale(zero=False), title='Density'),
    alt.Color('Month:N', sort=months, title='Month'),
    opacity=alt.value(.33)
).properties(
    height=450,
    width=600,
)

chart = box | kde
chart.configure_axis(
    labelFontSize=14,
    titleFontSize=20
).configure_title(
    fontSize=24
)

chart.save(
    str(PROJECT_DIR.joinpath('plots')
        .joinpath('06_multiple_distrobution.html')))
