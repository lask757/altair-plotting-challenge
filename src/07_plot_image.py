from pathlib import Path

import altair as alt
import pandas as pd

PROJECT_DIR = Path.cwd().parent
url = r'https://desertpy.com/images/new-desertpy-logo/Logo_DesertPy_ico.png'

data = {'x': [0, None], 'y': [0, None], 'url': [url, None]}

df = pd.DataFrame.from_dict(data)

img = alt.Chart(df).mark_image(
    width=400,
    height=400
).encode(
    alt.X('x', axis=None),
    alt.Y('y', axis=None),
    url='url'
).configure_axis(
    grid=False
).configure_view(
    strokeWidth=0
)

out = str(PROJECT_DIR.joinpath('plots').joinpath('07_plot_image.html'))
img.save(out)
