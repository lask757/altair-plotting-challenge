from pathlib import Path

import altair as alt
import pandas as pd

PROJECT_DIR = Path.cwd().parent
df = pd.read_excel(PROJECT_DIR.joinpath('data').joinpath('mars2019.xlsx'),
                   header=1, index_col=0, skipfooter=3, na_values='*')

df = df.drop('TOTAL', axis=1)
df = df.fillna(0, axis=0)

data = df.stack().reset_index()
data = data.rename(columns={'level_1': 'month', 0: 'marriages'})

chart = alt.Chart(data, height=1).transform_density(
    'marriages',
    groupby=['County'],
    as_=['marriages', 'density']
).mark_area(opacity=.9).encode(
    alt.X('marriages:Q', title='Marriages'),
    alt.Y('density:Q', scale=alt.Scale(range=[18, -18]), axis=None),
).properties(
    width=1_250
).facet(
    row=alt.Row('County:N',
                title=None,
                header=alt.Header(labelAngle=0, labelAlign='left'))
).configure_facet(
    spacing=0
).configure_view(
    stroke=None
)

chart.save(str(PROJECT_DIR.joinpath('plots').joinpath('08_ridge_polt.html')))
